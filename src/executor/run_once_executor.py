from .iexecutor import IExecutor
from akv import IAkvProvider
from selector import ISecretSelector
from receiver import ISecretReceiver
from log import Log

logger = Log.create(__name__)


class RunOnceExecutor(IExecutor):
    """
    An IExecutor which runs all the akvd appliction flow once and then returns
    """
    def __init__(
            self,
            akv_provider: IAkvProvider,
            secret_selector: ISecretSelector,
            secret_receiver: ISecretReceiver
    ):
        """
        :param akv_provider: the concrete IAkvProvider to use
        :param secret_selector: the concrete ISecretSelector to use
        :param secret_receiver: the concrete ISecretReceiver to use
        """
        self.akv_provider = akv_provider
        self.selector = secret_selector
        self.receiver = secret_receiver

    def execute(self):
        """
        Runs the executor. Uses akv_provider to list secrets, secret_selector to determine which secrets to fetch,
        akv_provider to fetch the selected secrets, and secret_receiver to receive the fetched secrets.
        :return: None
        """
        self.receiver.begin_update()

        try:
            secrets = self.akv_provider.list_secrets()
            for secret in secrets:
                logger.debug("processing secret `{0}`".format(secret))

                match = self.selector.is_secret_selected(secret)
                logger.debug("match={0}".format(match))

                if match[0]:
                    logger.debug("fetching secret `{0}`=>`{1}`".format(secret, match[2]))

                    secret_value = self.akv_provider.fetch(secret)
                    self.receiver.on_received_secret(match[2], secret_value)
                else:
                    logger.debug("skipping secret `{0}`".format(secret))
        except Exception as e:
            self.receiver.end_update()

            logger.error(e.__str__())
