#!/bin/sh

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

apt-get -y install python3-dev python3 python3-pip
pip3 install virtualenv

mkdir /opt/azure-key-vault-syncd
rm -R /opt/azure-key-vault-syncd/src
rm /opt/azure-key-vault-syncd/requirements.txt
rm /etc/systemd/system/azure-key-vault-syncd.service
cp -R ./* /opt/azure-key-vault-syncd

(cd /opt/azure-key-vault-syncd;
  virtualenv -p $(which python3) venv
  source ./venv/bin/activate
  pip3 install -r requirements.txt
)

adduser akvd --system --group

echo "akvd  ALL=(ALL) NOPASSWD:ALL" | tee /etc/sudoers.d/akvd

rm -R /srv/azure-key-vault
mkdir /srv/azure-key-vault
chown akvd:akvd /srv/azure-key-vault
chmod 0777 /srv/azure-key-vault

rm -R /dev/shm/akvd
mkdir /dev/shm/akvd
chown akvd:akvd /dev/shm/akvd
chmod 0777 /dev/shm/akvd

mkdir /etc/akvd
touch /etc/akvd/runtime.env

cp azure-key-vault-syncd.service /etc/systemd/system

systemctl daemon-reload

systemctl enable azure-key-vault-syncd.service

# systemctl start azure-key-vault-syncd.service