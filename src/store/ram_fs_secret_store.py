from configuration import AppSettings
from .isecret_store import ISecretStore
from marshmallow import Schema, fields, post_load
from .vendor import IRamFsCreator
from log import Log
import os
from util import expand

logger = Log.create(__name__)


class RamFsSecretStoreSettings(object):
    """
    Options for RamFsSecretStore.
    """
    def __init__(self, path: str, mount_to: str, allow_swap_on: bool):
        """
        :param path: path at which to create a RAM-based disk to store secrets. Only used when /dev/shm is not present
        :param mount_to: path to mount the RAM-based secret store to. e.g. /srv/azure-key-vault/secrets.
        :param allow_swap_on: whether to run akvd if system memory swapping is enabled.
        """
        self.path = expand(path)  # type: str
        self.mount_to = expand(mount_to)  # type: str
        self.allow_swap_on = allow_swap_on  # type: bool


class RamFsSecretStoreSettingsSchema(Schema):
    """
    Marshmallow schema for RamFsSecretStoreSettings
    """
    path = fields.Str(required=True, default="/tmp/azure-key-vault-syncd")
    mount_to = fields.Str(required=True, default="/srv/azure-key-vault/secrets")
    allow_swap_on = fields.Bool(default=False)

    @post_load
    def make_object(self, data) -> RamFsSecretStoreSettings:
        """
        Marshmallow post_load method to convert dict to RamFsSecretStoreSettings
        :param data: dict to convert
        :return: RamFsSecretStoreSettings
        """
        return RamFsSecretStoreSettings(**data)


class RamFsSecretStore(ISecretStore):
    """
    ISecretStore implementation which creates a RAM-based folder structure for storing secrets.
    """
    def __init__(self, app_settings: AppSettings, options: dict):
        super().__init__(app_settings, options)

        schema = RamFsSecretStoreSettingsSchema()
        self.options = schema.load(options).data  # type: RamFsSecretStoreSettings

        self.creator = IRamFsCreator.create()  # type: IRamFsCreator

        if not self.options.allow_swap_on:
            self.creator.assert_swap_off()

        self.creator.mount_ram_fs(self.options.path, self.options.mount_to)

    def save(self, category: str, name: str, user: str, group: str, data) -> None:
        """
        saves a secret to the secret store.

        :param category: the category of the secret (e.g. web, worker, etc.)
        :param name: name of the secret (e.g. mysql-connection-string)
        :param user: the OS user to secure the secret to
        :param group: the OS group to secure the secret to
        :param data: the value of the secret
        :return: None
        :raises: when the underlying file system handler raises
        """

        logger.info("saving secret `{0}` to category `{2}`".format(name, data, category))

        directory = os.path.join(self.options.mount_to, category)
        path = os.path.join(self.options.mount_to, category, name)

        if not os.path.exists(directory) or not os.path.isdir(directory):
            raise FileNotFoundError("path `{0}` does not exist or is not a directory".format(directory))

        logger.debug("opening path `{0}` for writing".format(path))
        try:
            secret_file = open(path + ".tmp", "w")
        except IOError as e:
            logger.error(e.__str__())
        else:
            with secret_file:
                secret_file.write(data)

            self.creator.set_owner(path + ".tmp", user, group, 0o770)
            self.creator.move(path + ".tmp", path)

    def evict(self, category: str, name: str):
        """
        removes a secret `name` from `category`
        :param category: the category from which to remove the secret
        :param name: the name of the secret to remove
        :return: None
        :raises: when the underlying file system handler raises
        """

        path = os.path.join(self.options.mount_to, category, name)

        if os.path.exists(path) and os.path.isfile(path):
            os.unlink(path)

    def ensure_secure(self, category: str, group: str):
        """
        ensures that a secret category can only be read by `group`.
        :param category: the category to secure
        :param group: the group to permit list access to
        :return:
        """

        path = os.path.join(self.options.mount_to, category)

        # 1.) create folder {self.options.mount_to}/category
        try:
            self.creator.mkdir(path, None, group, 0o2750)
        except:
            pass

        # 2.) assert uid and gid on folder
        self.creator.assert_user(path, None)
        self.creator.assert_group(path, group)

        # 3.) assert permissions 0o2750 on folder
        self.creator.assert_permissions(path, 0o2750)
