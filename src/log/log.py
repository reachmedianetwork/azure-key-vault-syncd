import logging
import platform


class HostnameFilter(logging.Filter):
    """
    a logging.Filter which adds the OS hostname to the log record.
    """
    hostname = platform.node()

    def filter(self, record):
        record.hostname = HostnameFilter.hostname
        return True


class Log(object):
    """
    Helper methods for creating named logger instances.
    """
    initialized = False

    @staticmethod
    def initialize() -> None:
        """
        Initializes the logging class with basicConfig.
        :return: None
        """
        if not Log.initialized:
            Log.initialized = True

            logging.basicConfig(level=logging.DEBUG)

    @staticmethod
    def create(name: str) -> logging.Logger:
        """
        Creates a logging.Logger with the specified name using the HostnameFilter
        :param name: logger name
        :return: logging.Logger
        """
        handler = logging.StreamHandler()
        handler.addFilter(HostnameFilter())
        handler.setFormatter(logging.Formatter(
            "<%(levelname)s> (%(hostname)s) %(asctime)s [%(name)s]\n    %(message)s",
            datefmt="%Y-%m-%d %H:%M:%S"
        ))

        logger = logging.getLogger(name)
        logger.addHandler(handler)
        logger.setLevel(logging.DEBUG)

        return logger
