import inspect
import re
import os
from typing import Match


def get_object_module_name(obj: object) -> str:
    """
    Gets the module name that defines the provided object.
    :param obj: object to inspect
    :return:
    """
    return next(filter(lambda item: item[0] == "__module__", inspect.getmembers(obj)))[1]


def replace_with_env(match: Match) -> str:
    """
    Replaces a regex match with a corresponding variable from env. The match should be in the format of %VAR% which
    would be replaced by an environment variable VAR. If no environment variable is found for the match, the original
    match, including the % signs is returned.

    :param match: the match to replace
    :return: str
    """
    m = match.group()  # type: str

    try:
        return os.environ[m[1:-1]]
    except KeyError:
        return m


def expand(string: str) -> str:
    """
    Expands environment variables found in `string` in the format of %NAME%
    :param string: string to expand
    :return: expanded string
    """
    return re.sub(r"%.*?%", replace_with_env, string)
