from configuration import AppSettings
from .isecret_receiver import ISecretReceiver
from store import ISecretStore
from marshmallow import Schema, fields, post_load
from typing import List, Tuple
import re
from log import Log

logger = Log.create(__name__)


class BucketSettings(object):
    """
    Options for a single destination bucket
    """
    def __init__(self, pattern: str, bucket: str, user: str, group: str):
        """
        Each bucket has a regex pattern which is used to determine if a secret belongs in that bucket. The regex *must*
        capture all or part of the input secret name. The captured text will be used as the secret name that is sent to
        the ISecretStore. For example:

        Given pattern is
            ^(?:web|common)-(?P<name>[0-9a-z-]+)
        The following secret names would result in the following output:
            web-dummy -> Tuple[True, 'dummy']
            common-a-secret-to-all -> Tuple[True, 'a-secret-to-all']
            worker-secret -> Tuple[False, 'worker-secret']

        The bucket name is passed to the ISecretStore as the "category". The exact resulting behavior will be dependent
        upon the ISecretStore, however, this will generally result in the secret being stored in a "directory" under
        the ISecretStore "root path" with the category name.

        :param pattern: the regular expression to use to determine if the secret belongs in this bucket
        :param bucket: name of the bucket
        :param user: the OS user to secure the bucket to
        :param group: the OS group to secure the bucket to
        """
        self.pattern = pattern
        self.bucket = bucket
        self.user = user
        self.group = group

    def is_secret_in_bucket(self, secret_name: str) -> Tuple[bool, str]:
        """
        Determines if a secret should be included in the bucket
        :param secret_name: secret name
        :return: Tuple describing if the secret is included and if so, the (possibly) augmented name of the secret
        """
        matches = re.findall(self.pattern, secret_name)
        if len(matches) > 0:
            return True, matches[0]

        return False, secret_name


class BucketSettingsSchema(Schema):
    """
    Marshmallow schema for BucketSettings
    """
    pattern = fields.Str()
    bucket = fields.Str()
    user = fields.Str()
    group = fields.Str()

    @post_load
    def make_object(self, data) -> BucketSettings:
        """
        Marshmallow post_load method to convert dict to BucketSettings
        :param data: dict to convert
        :return: BucketSettings
        """
        return BucketSettings(**data)


class BucketSecretReceiverSettings(object):
    """
    Options for BucketSecretReceiver
    """
    def __init__(self, buckets: List[BucketSettings]):
        """
        :param buckets: list of BucketSettings objects
        """
        self.buckets = buckets


class BucketSecretReceiverSettingsSchema(Schema):
    """
    Marshmallow schema for BucketSecretReceiverSettings
    """
    buckets = fields.List(fields.Nested(BucketSettingsSchema()))

    @post_load
    def make_object(self, data) -> BucketSecretReceiverSettings:
        """
        Marshmallow post_load method to convert dict to BucketSecretReceiverSettings
        :param data: dict to convert
        :return: BucketSecretReceiverSettings
        """
        return BucketSecretReceiverSettings(**data)


class BucketSecretReceiver(ISecretReceiver):
    """
    ISecretReceiver which filters secrets into 0 or more "buckets" and stores them in the ISecretStore

    TODO: BucketSecretReceiver should "purge" secrets from ISecretStore that existed in the preceding update cycle
          but which did not exist in the current update cycle. Although, maybe we should consider that carefully. What
          if IAkvProvider just fails to get an accurate list of secrets? Then we would obliterate a legitimate secret,
          yeah?
    """
    def __init__(self, store: ISecretStore, app_settings: AppSettings, options: dict):
        super().__init__(store, app_settings, options)

        self.store = store

        self.old_secret_files = set()  # type: set
        self.secret_files = set()  # type: set

        schema = BucketSecretReceiverSettingsSchema()
        self.options = schema.load(options).data  # type: BucketSecretReceiverSettings

        # print([bucket.pattern for bucket in self.options.buckets])
        for bucket in self.options.buckets:
            logger.info(
                "establishing secure secret bucket `{0}` for user `{1}` having pattern `{2}`"
                .format(bucket.bucket, bucket.user, bucket.pattern)
            )
            store.ensure_secure(bucket.bucket, bucket.user)

    def begin_update(self) -> None:
        """
        handles the "begin_update" event. This occurs at the beginning of an update cycle.
        :return: None
        """
        self.old_secret_files.clear()
        self.old_secret_files.update(self.secret_files)

        self.secret_files.clear()

    def end_update(self) -> None:
        """
        handles the "end_update" event. This occurs at the end of an update cycle after all secrets have been fetched.
        :return: None
        """
        for secret in enumerate(self.old_secret_files):
            # TODO: delete file
            logger.warning("todo: delete file " + secret[1])

        self.old_secret_files.clear()

    def on_received_secret(self, secret_name: str, secret_value: str) -> None:
        """
        handles the "on_received_secret" event. This occurs once for each secret IAkvProvider fetches from AKV
        :param secret_name: name of the secret
        :param secret_value: the decoded string value fo the secret
        :return: None
        """
        logger.debug("receiving secret `{0}`".format(secret_name))

        for bucket in self.options.buckets:
            logger.debug("checking if secret should be saved to bucket `{0}`".format(bucket.bucket))
            match = bucket.is_secret_in_bucket(secret_name)
            logger.debug("match={0}".format(match))

            if match[0]:
                logger.debug("saving secret `{0}` to bucket `{1}`".format(secret_name, bucket.bucket))
                self.store.save(bucket.bucket, match[1], bucket.user, bucket.group, secret_value)
