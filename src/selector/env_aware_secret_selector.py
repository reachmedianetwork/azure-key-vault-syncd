from typing import Tuple, Pattern
import re
from .isecret_selector import ISecretSelector
from configuration import AppSettings
from marshmallow import Schema, fields, post_load
from log import Log
from util import expand

logger = Log.create(__name__)


class EnvAwareSecretSelectorSettings(object):
    """
    Options for EnvAwareSecretSelector
    """
    def __init__(self, pattern: str):
        """
        The EnvAwareSecretSelector takes a regex pattern which is used to determine if a secret should be selected and
        sent to the ISecretReceiver. The regex *must* capture the name of the secret that should be sent to the
        ISecretReceiver.

        This selector will "expand" the regex pattern with variables from env. For example:

        Given the follow pattern:
            ^%RCH_ENV%-(?P<name>(?:%RCH_ROLE%|common)-[0-9a-z-]+)
        And given the follow environment variables:
            RCH_ENV="prd"
            RCH_ROLE="web"
        The effective regex pattern that EnvAwareSecretSelector will use will be:
            ^prd-(?P<name>(?:web|common)-[0-9a-z-]+)

        It is worth noting that when akvd is installed as a systemd unit using the accompanying .service file, that
        the contents of /etc/akvd/runtime.env will be loaded into the service env. This makes for a good companion
        configuration modality of "augmenting" this pattern setting with env variables.

        :param pattern: regex
        """
        self.pattern = pattern
        self.expanded_pattern = expand(pattern)  # type: str
        self.regex = re.compile(self.expanded_pattern)  # type: Pattern

    def match(self, secret_name: str) -> Tuple[bool, str, str]:
        """
        Returns a tuple indicating if the secret_name matches this option's regex pattern and if so, the original secret
        name and the captured secret name from the regular expression.
        :param secret_name:
        :return:
        """
        matches = self.regex.findall(secret_name)
        if len(matches) > 0:
            return True, secret_name, matches[0]

        return False, secret_name, secret_name


class EnvAwareSecretSelectorSettingsSchema(Schema):
    """
    Marshmallow schema for EnvAwareSecretSelectorSettings
    """
    pattern = fields.Str(required=True)

    @post_load
    def make_object(self, data) -> EnvAwareSecretSelectorSettings:
        """
        Marshmallow post_load method to convert dict to EnvAwareSecretSelectorSettings
        :param data: dict to convert
        :return: EnvAwareSecretSelectorSettings
        """
        return EnvAwareSecretSelectorSettings(**data)


class EnvAwareSecretSelector(ISecretSelector):
    """
        A secret selector that uses environment variables to make selections
    """
    def __init__(self, app_settings: AppSettings, options: dict):
        super().__init__(app_settings, options)

        schema = EnvAwareSecretSelectorSettingsSchema()
        self.options = schema.load(options).data  # type: EnvAwareSecretSelectorSettings

    def is_secret_selected(self, secret_name) -> Tuple[bool, str, str]:
        """
        method called to determine if a secret should be selected (fetched) from AKV. Returns a tuple indicating if the
        secret_name matches this selector's regex pattern and if so, the original secret name and the captured secret
        name from the regular expression.
        :param secret_name: secret name to test
        :return: Tuple[bool, str, str]
        """
        return self.options.match(secret_name)
