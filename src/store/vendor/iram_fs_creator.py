import abc
import sys
from log import Log
from typing import Optional
from util import get_object_module_name

logger = Log.create(__name__)


class IRamFsCreator:
    """
    Defines implementation requirements for RAM-based disk management.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def assert_swap_off(self) -> None:
        """
        If the underlying OS is susceptible to swapping contents of RAM-based disks, this method should assert that
        memory swapping is turned off.
        :return: None
        :raises: AssertionError when the OS configuration might lead to leaking to swap space
        """
        pass

    @abc.abstractmethod
    def mount_ram_fs(self, path: str, mount_point: str) -> None:
        """
        Creates a ram disk at `path` and mounts it at `mount_point`

        :param path: path to create RAM disk at
        :param mount_point: path to symlink new RAM disk to
        :return: None
        :raises: AssertionError when configuration parameters cannot be met
        """
        pass

    @abc.abstractmethod
    def mkdir(self, path: str, user: Optional[str], group: Optional[str], permissions: int) -> None:
        """
        creates a directory at path with user, group, and permissions. If the directory already exists, the user, group,
        and permissions will be updated as needed.

        If user or group is None, the respective user or group of akvd will be resolved and used.

        :param path: path to create
        :param user: user to own the new directory. If None, application's user will be used
        :param group: group to own the new directory. If None, application's group will be used
        :param permissions: unix-style octal to set for the new directory
        :raises when the directory cannot be created, ownership cannot be set, or permissions cannot be set
        """
        pass

    def set_owner(self, path: str, user: Optional[str], group: Optional[str], permissions: int) -> None:
        """
        sets the user, group, and mode of the specified file.

        If user or group is None, the respective user or group of akvd will be resolved and used.

        :param path: file to change
        :param user: OS user to chown the file to
        :param group: OS group to chown the file to
        :param permissions: the Linux octal file mode to apply
        :return: None
        :raises: AssertionError if path does not exist or is a directory
        """
        pass

    def move(self, from_path: str, to_path: str):
        """
        moves a file.
        :param from_path: source path
        :param to_path: destination path
        :return: None
        """
        pass

    @abc.abstractmethod
    def assert_user(self, path: str, user: Optional[str]):
        """
        asserts that a path is owned by a specific OS user.

        If user is None, the user akvd is running as will be resolved and used.

        :param path: the path to assert
        :param user: OS user to ensure owns path
        :return: None
        :raises: AssertionError if path is not owned by user
        """
        pass

    @abc.abstractmethod
    def assert_group(self, path: str, group: Optional[str]):
        """
        asserts that a path is owned by a specific OS group.

        If group is None, the group akvd is running as will be resolved and used.

        :param path: the path to assert
        :param group: OS group to ensure owns path
        :return: None
        :raises: AssertionError if path is not owned by group
        """
        pass

    @abc.abstractmethod
    def assert_permissions(self, path: str, permissions: int):
        """
        asserts that a path has the specified mode.

        :param path: the path to assert
        :param permissions: Linux file system mode to assert
        :return: None
        :raises: AssertionError if path does not have mode
        """
        pass

    @staticmethod
    def create():
        # type: () -> IRamFsCreator
        """
        Creates an IRamFsCreator instance that is appropriate for the current OS. Presently, only Linux is supported.
        :return: IRamFsCreator
        """
        cls = None

        if sys.platform.startswith("linux"):
            module = __import__("store").vendor.linux
            cls = getattr(module, "LinuxRamFsCreator")

        # TODO: implement Windows
        # if sys.platform.startswith("win32"):
        #     module = __import__("store").vendor.windows
        #     cls = getattr(module, "ImDiskRamFsCreator")

        if cls is None:
            raise NotImplementedError("no IRamFsCreator is implemented for platform {0}".format(sys.platform))

        logger.debug(
            "using IRamFsCreator `{0}` from module {1}"
            .format(cls.__name__, get_object_module_name(cls))
        )

        return cls()
