from store.vendor import IRamFsCreator
import psutil


class ImDiskRamFsCreator(IRamFsCreator):
    """
    An IRamFsCreator implementation for Windows. This class uses ImDisk to manage the creation and mounting of RAM disks
    TODO: implement
    """
    def mkdir(self, path: str, user: str, group: str, permissions: int):
        raise NotImplementedError

    def assert_user(self, path: str, user: str):
        raise NotImplementedError

    def assert_group(self, path: str, group: str):
        raise NotImplementedError

    def assert_permissions(self, path: str, permissions: int):
        raise NotImplementedError

    def assert_swap_off(self):
        """
        Asserts that memory swapping is disabled.
        :raises: AssertionError
        """
        if psutil.swap_memory().total > 0:
            raise AssertionError

    def mount_ram_fs(self, path: str, mount_point: str):
        raise NotImplementedError
