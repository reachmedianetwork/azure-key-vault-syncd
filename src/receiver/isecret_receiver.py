import abc
from configuration import AppSettings
from store import ISecretStore


class ISecretReceiver:
    """
    Defines the implementation requirements for an ISecretReceiver. An ISecretReceiver must handle the "begin_update",
    "on_received_secret", and "end_update" events which make up an update cycle.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, store: ISecretStore, app_settings: AppSettings, options: dict):
        """
        Silly hack to force concretions to accept AppSettings and kwargs
        :param store: the ISecretStore to use
        :param app_settings: AppSettings to use
        :param options: ISecretReceiver provider options
        """
        pass

    @abc.abstractmethod
    def begin_update(self) -> None:
        """
        This method is called when a update cycle begins but before any secrets have been fetched from AKV
        :return: None
        """
        pass

    @abc.abstractmethod
    def end_update(self) -> None:
        """
        This method is called after the last secret has been relayed for an update cycle.
        :return: None
        """
        pass

    @abc.abstractmethod
    def on_received_secret(self, secret_name: str, secret_value: str) -> None:
        """
        This method is called once for each secret that is enumerated, selected, and fetched during an update cycle.
        :param secret_name: the name of the secret as presented by the ISecretSelector
        :param secret_value: the decoded value of the secret
        :return:
        """
        pass
