from .iexecutor import IExecutor
from threading import Event
import signal
from log import Log

logger = Log.create(__name__)
exit_signal = Event()


def handle_sig(sig_no: int, _frame):
    """
    method to receive OS signals. Sets the exit_signal synchronization object which indicates the application should
    terminate.
    :param sig_no: signal number being raised by OS
    :param _frame: stack frame
    :return: None
    """
    logger.info("interrupted by {0}, shutting down".format(sig_no))

    exit_signal.set()


# register handler for SIGTERM, SIGHUP, and SIGINT
for sig in ('TERM', 'HUP', 'INT'):
    signal_num = getattr(signal, 'SIG' + sig)

    logger.debug("handling SIG{0} ({1})".format(sig, signal_num))

    signal.signal(signal_num, handle_sig)


class DaemonExecutor(IExecutor):
    """
    An IExecutor concretion which runs on an interval
    """
    def __init__(self, interval: int, executor: IExecutor):
        """
        :param interval: interval, in seconds, to run
        :param executor: the real executor to invoke on each cycle
        """
        self.interval = interval
        self.executor = executor

    def execute(self):
        """
        Runs the executor
        :return: None
        """
        logger.info("daemon executor running with an interval of {0} second(s)".format(self.interval))

        while not exit_signal.is_set():
            logger.info("refreshing secrets from vault")

            self.executor.execute()
            exit_signal.wait(self.interval)
