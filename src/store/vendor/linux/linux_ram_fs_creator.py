from store.vendor import IRamFsCreator
import subprocess32 as subprocess
import psutil
import os
import getpass
import sys
import re
import stat
from typing import Optional
from log import Log

logger = Log.create(__name__)
grp = __import__("grp")
pwd = __import__("pwd")
FNULL = open(os.devnull, 'w')


class LinuxRamFsCreator(IRamFsCreator):
    """
    An IRamFsCreator implementation for Linux
    """

    def move(self, from_path: str, to_path: str) -> None:
        """
        moves a file.
        :param from_path: source path
        :param to_path: destination path
        :return: None
        """
        subprocess.check_call(["sudo", "mv", "-f", from_path, to_path], stdout=FNULL)

    def set_owner(self, path: str, user: Optional[str], group: Optional[str], permissions: int) -> None:
        """
        sets the user, group, and mode of the specified file.

        If user or group is None, the respective user or group of akvd will be resolved and used.

        :param path: file to change
        :param user: OS user to chown the file to
        :param group: OS group to chown the file to
        :param permissions: the Linux octal file mode to apply
        :return: None
        :raises: AssertionError  if path does not exist or is a directory
        """
        logger.debug("set_owner({0}, {1}, {2}, {3})".format(path, user, group, permissions))

        if not os.path.exists(path):
            raise AssertionError("path `{0}` does not exist".format(path))
        elif os.path.isdir(path):
            raise AssertionError("path `{0}` is a directory".format(path))

        if user is None:
            uid = os.getuid()
            user = pwd.getpwuid(uid).pw_name
        else:
            uid = pwd.getpwnam(user).pw_uid

        if group is None:
            gid = os.getgid()
            group = grp.getgrgid(gid).gr_name
        else:
            gid = grp.getgrnam(group).gr_gid

        logger.debug("resolved uid for {0}={1}, guid for {2}={3}".format(user, uid, group, gid))

        file_stat = os.stat(path)
        file_uid = file_stat.st_uid
        file_gid = file_stat.st_gid
        file_mode = stat.S_IMODE(file_stat.st_mode)

        if file_uid != uid or file_gid != gid:
            logger.debug(
                "path `{0}` has incorrect uid {1} or gid {2}; setting to uid {3}, guid {4}"
                .format(path, file_uid, file_gid, uid, gid)
            )

            subprocess.check_call(["sudo", "chown", "{0}:{1}".format(user, group), path], stdout=FNULL)

        if file_mode != permissions:
            logger.debug(
                "path `{0}` has incorrect mode {1}; setting to {2}"
                .format(path, oct(file_mode), oct(permissions))
            )

            subprocess.check_call(["sudo", "chmod", oct(permissions).replace("o", ""), path], stdout=FNULL)

    def mkdir(self, path: str, user: Optional[str], group: Optional[str], permissions: int) -> None:
        """
        creates a directory with user, group, and permissions. If the directory already exists, the user, group, and
        mode will be updated accordingly.

        If user or group is None, the respective user or group of akvd will be resolved and used.

        :param path: the path to create
        :param user: OS user to own the directory
        :param group: OS group to own the directory
        :param permissions: the Linux directory mode to apply
        :return: None
        :raises: AssertionError when path exists but is not a directory
        """
        logger.debug("mkdir({0}, {1}, {2}, {3})".format(path, user, group, permissions))

        if not os.path.exists(path):
            logger.debug("`{0}` does not exist, creating".format(path))
            os.mkdir(path, permissions)
        elif not os.path.isdir(path):
            raise AssertionError("path `{0}` is not a directory".format(path))

        if user is None:
            uid = os.getuid()
            user = pwd.getpwuid(uid).pw_name
        else:
            uid = pwd.getpwnam(user).pw_uid

        if group is None:
            gid = os.getgid()
            group = grp.getgrgid(gid).gr_name
        else:
            gid = grp.getgrnam(group).gr_gid

        logger.debug("resolved uid for {0}={1}, guid for {2}={3}".format(user, uid, group, gid))

        dir_stat = os.stat(path)
        dir_uid = dir_stat.st_uid
        dir_gid = dir_stat.st_gid
        dir_mode = stat.S_IMODE(dir_stat.st_mode)

        if dir_uid != uid or dir_gid != gid:
            logger.debug(
                "path `{0}` has incorrect uid {1} or gid {2}; setting to uid {3}, guid {4}"
                .format(path, dir_uid, dir_gid, uid, gid)
            )

            subprocess.check_call(["sudo", "chown", "{0}:{1}".format(user, group), path], stdout=FNULL)

        if dir_mode != permissions:
            logger.debug(
                "path `{0}` has incorrect mode {1}; setting to {2}"
                .format(path, oct(dir_mode), oct(permissions))
            )

            subprocess.check_call(["sudo", "chmod", oct(permissions).replace("o", ""), path], stdout=FNULL)

    def assert_user(self, path: str, user: Optional[str]) -> None:
        """
        asserts that a path is owned by a specific OS user.

        If user is None, the user akvd is running as will be resolved and used.

        :param path: the path to assert
        :param user: OS user to ensure owns path
        :return: None
        :raises: AssertionError if path is not owned by user
        """

        if user is None:
            uid = os.getuid()
        else:
            uid = pwd.getpwnam(user).pw_uid

        dir_stat = os.stat(path)
        dir_uid = dir_stat.st_uid

        if uid != dir_uid:
            raise AssertionError("path `{0}` has uid {1} but uid {2} is required".format(path, dir_uid, uid))

    def assert_group(self, path: str, group: Optional[str]) -> None:
        """
        asserts that a path is owned by a specific OS group.

        If group is None, the group akvd is running as will be resolved and used.

        :param path: the path to assert
        :param group: OS group to ensure owns path
        :return: None
        :raises: AssertionError if path is not owned by group
        """

        if group is None:
            gid = os.getgid()
        else:
            gid = grp.getgrnam(group).gr_gid

        dir_stat = os.stat(path)
        dir_gid = dir_stat.st_gid

        if gid != dir_gid:
            raise AssertionError("path `{0}` has gid {1} but gid {2} is required".format(path, dir_gid, gid))

    def assert_permissions(self, path: str, permissions: int):
        """
        asserts that a path has the specified mode.

        :param path: the path to assert
        :param permissions: Linux file system mode to assert
        :return: None
        :raises: AssertionError if path does not have mode
        """

        dir_stat = os.stat(path)
        dir_mode = stat.S_IMODE(dir_stat.st_mode)

        if dir_mode != permissions:
            raise AssertionError(
                "path `{0}` has mode {1} but mode {2} is required".format(path, oct(dir_mode), oct(permissions))
            )

    def assert_swap_off(self):
        """
        Asserts that memory swapping is disabled on the system.
        :raises: AssertionError
        """
        memory = psutil.swap_memory()

        logger.debug("swap_memory: {0}".format(memory))

        if memory.total > 0:
            logger.error("swap memory is enabled")
            raise AssertionError

    @staticmethod
    def get_mounted_fs_type_for_path(path: str):
        """
        Attempts to get the FS type of a mount point.
        :param path: path to get FS type for
        :return: FS type as a string (e.g. ext4, ramfs, tmpfs, etc.)
        """
        logger.debug("acquiring fs type for path {0}".format(path))

        pattern = r"^(?P<type>.*) on (?P<path>.*) type \1 \((?P<options>.*)\)$"
        mounts = subprocess.check_output(["mount"]).decode(sys.stdout.encoding)  # type: str

        for mount in mounts.splitlines():
            matches = re.match(pattern, mount)

            if matches is not None:
                match = matches.groupdict()
                if match["path"] == path:
                    return match["type"]

        return None

    @staticmethod
    def mount_shm_fs(mount_point: str) -> None:
        """
        Configures /dev/shm/akvd for use as the RAM-based secret store.

        This method will create /dev/shm/akvd directory if it does not exist.

        :param mount_point: path to symlink /dev/shm/akvd to
        :return: None
        :raises: AssertionError if /dev/shm/akvd is not owned by user and group akvd is running as
        :raises: AssertionError if mount_point does not resolve to /dev/shm/akvd
        """
        if not os.path.exists("/dev/shm/akvd"):
            logger.info("path `/dev/shm/akvd` does not exist; creating")

            os.mkdir("/dev/shm/akvd")
            os.chmod("/dev/shm/akvd", 0o777)
        else:
            logger.info("path `/dev/shm/akvd` already exists")

            my_uid = os.getuid()
            my_gid = os.getgid()
            file_stat = os.stat("/dev/shm/akvd")
            file_uid = file_stat.st_uid
            file_gid = file_stat.st_gid
            file_mode = oct(stat.S_IMODE(file_stat.st_mode))

            logger.debug("user/stat for `/dev/shm/akvd`: {0}".format({
                "my_uid": my_uid,
                "my_gid": my_gid,
                "file_uid": file_uid,
                "file_gid": file_gid,
                "file_mode": file_mode
            }))

            if my_uid != file_uid:
                raise AssertionError("path `/dev/shm/akvd` must have my uid ({0})".format(my_uid))

            if my_gid != file_gid:
                raise AssertionError("path `/dev/shm/akvd` must have my gid ({0})".format(my_gid))

        if not os.path.exists(mount_point):
            logger.info("symlink `{0}` does not exist; creating".format(mount_point))

            os.symlink("/dev/shm/akvd", mount_point, True)
            os.chmod(mount_point, 0o777)
        else:
            logger.info("symlink `{0}` already exists".format(mount_point))

            resolved_path = os.path.realpath(mount_point)
            logger.debug("symlink `{0}` resolves to `{1}`".format(mount_point, resolved_path))

            if resolved_path != "/dev/shm/akvd":
                raise AssertionError("symlink `{0}` must resolve to `/dev/shm/akvd`".format(resolved_path))

    def mount_ram_fs(self, path: str, mount_point: str) -> None:
        """
        Creates a new ramfs file system at path and symlinks it to mount_point.

        If /dev/shm is present on the system, this function will use mount_shm_fs instead of creating a new ramfs.

        :param path: path to create new ramfs at
        :param mount_point: path to symlink new ramfs to
        :return: None
        :raises: AssertionError if /dev/shm exists but is not a ramfs or tmpfs
        :raises: AssertionError if path already exists but is not a ramfs
        """
        if os.path.exists("/dev/shm"):
            logger.info("/dev/shm is present on this system; using that")
            existing_fs_type = LinuxRamFsCreator.get_mounted_fs_type_for_path("/dev/shm")

            logger.debug("/dev/shm is of fs type " + str(existing_fs_type))

            if existing_fs_type != "ramfs" and existing_fs_type != "tmpfs":
                raise AssertionError(
                    "/dev/shm has fs type {0} but must be either ramfs or tmpfs".format(existing_fs_type)
                )

            LinuxRamFsCreator.mount_shm_fs(mount_point)
        else:
            logger.info("/dev/shm is not present on this system; creating a new ramfs mount")

            user = getpass.getuser()
            gid = os.getegid()

            logger.debug("using user={0}, gid={1}".format(user, gid))

            # create the folder that we will mount the ram disk to
            if not os.path.exists(path):
                logger.debug("creating directory `{0}`".format(path))

                os.mkdir(path)

            existing_fs_type = LinuxRamFsCreator.get_mounted_fs_type_for_path(path)
            if existing_fs_type is None:
                # mount the ram disk
                params = ["mount", "-t", "ramfs", "ramfs", path]
                logger.debug("executing subprocess `{0}`".format(" ".join(params)))
                subprocess.check_output(params)

                params = ["chown", "{0}:{1}".format(user, gid), path]
                logger.debug("executing subprocess `{0}`".format(" ".join(params)))
                subprocess.check_output(params)

                # restrict access to this folder to current user and group only
                logger.debug("setting permissions to 0700 for path `{0}`".format(path))
                os.chmod(path, 0o777)
            elif existing_fs_type != "ramfs":
                raise TypeError("path {0} is not a ramfs mount".format(path))

            if path != mount_point:
                # symlink it somewhere else
                logger.info("symlink `{0}` does not exist; creating".format(mount_point))

                os.symlink(path, mount_point, True)
                os.chmod(mount_point, 0o777)
