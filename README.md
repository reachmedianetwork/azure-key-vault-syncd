# Azure Key Vault Syncd (_akvd_)

This is a tool designed to synchronize secrets stored in _Azure Key Vault_ to a
secure RAM-based disk on the local machine. Design goals for this tool are:

* __Security__. Secrets should be exposed to only the application needing them.
* __Interoperability__. Secrets should be easy to consume.
* __Extensibility__. The application should allow for customization

This application does not:

* Sync `keys` or `certificates` from _Azure Key Vault_

TL;DR

You might want to check out the __Deployment__ and __Configuration__ sections to
get going.

## Security Features

The following security considerations were taken:

* Secrets are stored in volatile memory and not on disk.
    * The application uses _tmpfs_ via `/dev/shm`
    * The application will not run if swapping is enabled
    * WARNING: the application only checks if swap is enabled at start up. If
      someone enables swap after that point, it is possible that secrets will be
      written to the swap space. So I guess you shouldn't enable swapping after
      this service starts...
* _akvd_ is designed to only pull secrets relevant to the machine.
* The default `ISecretReceiver` will partition secrets into "buckets". These
  buckets are secured for use by a specific user/group combination. A secret
  can be saved to multiple buckets.
* The default `IAkvProvider` uses Azure Instance Metadata Service to acquire an
  access token for use with Azure Key Vault. The VM that _akvd_ is running on
  must have a managed identity assigned to it and that identity must be given
  rights to read secrets from the target Azure Key Vault

### Security: Concerns Not Addressed

Once the secrets are written to the RAM-based disk, they can be read by any
process running as the user/group for which they are intended. No mechanism
exists to supply secrets to a specific process or executable.

Further, if the application that has been granted access to the secrets is
compromised (e.g. the application is a PHP script which has an exploit), the
secrets could be read. For example, this script could be used to read a secret.

```PHP
<?php

$file_to_read = $_REQUEST['file_to_read'];
echo file_get_contents($file_to_read);
```

A user could then invoke the script like so:

```shell
curl http://path/to/vuln.php?file_to_read=%2Fsrv%2Fazure-key-vault-secrets%2Fweb%2Fsecure-secret
```

This security vulnerability is extremely obvious. No one in the real world
would ever use un-sanitized user input like that. Right?

Once a secret is read from secure storage, it is incumbent upon the consumer to
maintain the security of that secret. Enough said.

## Interoperability

This solution needs to provide secrets to consumers in a way that is supported
by most consumers. Most things will have the ability to read files from the
file system, so that seemed like the logical place to put the secrets.

This may not meet every possible use-case. In this case, check out the
__Extensibility__ section of this document to learn how you could expose secrets
in another way.

## Extensibility

_akvd_ is fairly extensible. To understand possible extensibility points, the
application flow must first be understood.

|    Application Flow   |
|:---------------------:|
|       `__main__`      |
|           ↓           |
|      `IExecutor`      |
|           ↓           |
|  `IAkvProvider (e)`   |
|           ↓           |
| `ISecretSelector (e)` |
|           ↓           |
| `ISecretReceiver (e)` |
|           ↓           |
|   `ISecretStore (e)`  |

Items denoted `(e)` are extensibility points, meaning that they can be swapped
out with a custom implementation.

### IAkvProvider

The `IAkvProvider` component is responsible for interacting with the _Azure Key
Vault_. It provides two main functions:

* `list_secrets()`: enumerates all secrets in the configured Key Vault
* `fetch(name)`: retrieves the secret value for a specific secret

#### ManagedIdentityAkvProvider

The default implementation of `IAkvProvider` is
`akv.ManagedIdentityAkvProvider`, which uses the _Azure Instance Metadata
Service (AIMS)_ to retrieve an `access_token` for _Azure Key Vault_. In order
for this to work, the VM that _akvd_ is running on must have a managed identity
assigned to it and that managed identity must have sufficient rights to list and
read secrets in the _Azure Key Vault_.

Refer to [Microsoft's tutorial][tutorial-linux-vm-access-nonaad] on how to use a
Linux VM system-assigned managed identity to access _Azure Key Vault_.

[tutorial-linux-vm-access-nonaad]: https://docs.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/tutorial-linux-vm-access-nonaad "Tutorial: Use a Linux VM system-assigned managed identity to access Azure Key Vault"

`ManagedIdentityAkvProvider` takes the following configuration:

```json
...
"akv_provider": {
    "name": "ManagedIdentityAkvProvider",
    "options": {
        "vault_url": "https://php-secrets.vault.azure.net/"
    }
}
...
```

The `options.vault_url` value will be expanded with environment variables which
allows for env vars to be injected. For example:

Given a `/etc/akvd/runtime.env` file with the following contents:

```shell
RCH_ENV="prd"
RCH_ROLE="web"
```

And a `options.vault_url` value of `"https://%RCH_ENV%-php-secrets.azure.net/"`,
the effective value will be `"https://prd-php-secrets.azure.net/"`.

### ISecretSelector

The `ISecretSelector` component is responsible for determining which secrets in
the vault will be synchronized to the local machine. The `ISecretSelector`
implements one method:

```python
def is_secret_selected(self, secret_name) -> Tuple[bool, str, str]:
```

During each update cycle, this method is invoked for each secret that is found
in the vault. This method returns a tuple with three values:

1. `bool`: _True_ if the secret should be synchronized, _False_ otherwise.
2. `str`: the original name of the secret
3. `str`: the new name of the secret

The `ISecretSelector` may, optionally, augment the name of the secret before it
is passed to the `ISecretReceiver`.

#### EnvAwareSecretSelector

The `EnvAwareSecretSelector` class is an implementation of `ISecretSelector`
that uses a regular expression to determine if a secret should be selected. The
regular expression will be "expanded" with environment variables. Here is an
example configuration:

```json
...
"secret_selector": {
    "name": "EnvAwareSecretSelector",
    "options": {
        "pattern": "^%RCH_ENV%-(?P<name>(?:%RCH_ROLE%|common)-[0-9a-z-]+)"
    }
}
...
```

And let's imagine that `/etc/akvd/runtime.env` has these contents:

```shell
RCH_ENV="prd"
RCH_ROLE="web"
```

And let's imagine that the Key Vault has the following secrets defined:

```shell
prd-web-jwt-encryption-key
prd-common-sql-connection-string
dev-web-sql-connection-string
prd-worker-clover-connection-string
```

In this case, `%RCH_ENV%` and `%RCH_ROLE%` will be replaced in the pattern with
the appropriate environment variables. It will become:

```regex
^prd-(?P<name>(?:web|common)-[0-9a-z-]+)
```

This expanded regular expression will match on:

```shell
prd-web-jwt-encryption-key
prd-common-sql-connection-string
```

`EnvAwareSecretSelector` will then use the `name` named capture group as the
secret name to store locally. So the above two secrets would become:

```shell
web-jwt-encryption-key
common-sql-connection-string
```

### ISecretReceiver

The `ISecretReceiver` component receives the secrets from the `ISecretSelector`.
This component implements the following functions:

```python
def begin_update(self):
def end_update(self):
def on_received_secret(self, secret_name: str, secret_value: str):
```

An update cycle follows this pattern:

1. `begin_update()`
2. for each selected secret, `on_received_secret(secret_name, secret_value)`
3. `end_update()`

It is the responsibility of `ISecretReceiver` to relay secrets to
`ISecretStore`.

#### BucketSecretReceiver

The class `BucketSecretReceiver` is the default `ISecretReceiver` implementation
used by _akvd_.

Given this configuration:

```json
...
"secret_receiver": {
    "name": "BucketSecretReceiver",
    "options": {
        "buckets": [
            {
                "pattern": "^(?:web|common)-(?P<name>[0-9a-z-]+)",
                "bucket": "web",
                "user": "www-data",
                "group": "www-data"
            },
            {
                "pattern": "^(?:worker|common)-(?P<name>[0-9a-z-]+)",
                "bucket": "worker",
                "user": "root",
                "group": "root"
            }
        ]
    }
}
...
```

`BucketSecretReceiver` will iterate through each bucket configuration. If the
secret name (as provided by `ISecretSelector`) matches a bucket regex pattern,
the secret will be written to the secret bucket. The secret name will be
whatever value is in the `name` capture group. So, presuming that the receiver
gets the following two secrets:

```shell
web-jwt-encryption-key
common-sql-connection-string
```

It will write the following secrets:

* `jwt-encryption-key` to bucket "web", which gives the user/group `www-data`
  access to the secret.
* `sql-connection-string` to bucket "web", which gives the user/group `www-data`
  access to the secret.
* `sql-connection-string` to bucket "worker", which gives the user/group `root`
  access to the secret.

### ISecretStore

The `ISecretStore` component is responsible for writing the secret somewhere.
The default implementation is `RamFsSecretStore`.

#### RamFsSecretStore

The `RamFsSecretStore` implementation of `ISecretStore` writes the secrets to a
secure RAM-based directory.  If `/dev/shm` is available, it creates a folder
there. Otherwise, a new _ramfs_ volume is created.

`RamFsSecretStore` ensures that memory swapping is disabled to prevent secrets
from being swapped to disk and therefore possibly leaked. This is admittedly
a fairly improbable way for secrets to be exposed, but still a loose thread
that ought to have been dealt with.

`RamFsSecretStore` takes the following configuration:

```json
...
"secret_store": {
    "name": "RamFsSecretStore",
    "options": {
        "path": "/tmp/azure-key-vault-syncd",
        "mount_to": "/srv/azure-key-vault/secrets",
        "allow_swap_on": false
    }
}
...
```

* `options.path`: this specifies where the RAM-based folder should be hosted.
  If `/dev/shm` is available on the system, this option will be ignored.
* `options.mount_to`: this specifies where the RAM-based folder will be
  symlinked to. This is where consumers should look for secrets.
* `options.allow_swap_on`: this is an override option that allows _akvd_ to run
  even if memory swapping is enabled. If `/dev/shm` is used, this could result
  in leaking of secrets to the disk swap space. This may be an acceptable
  security trade-off.

## Configuration

The default configuration uses the following concrete implementations:

* __IAkvProvider__: `akv.ManagedIdentityAkvProvider`
* __ISecretSelector__: `selector.EnvAwareSecretSelector`
* __ISecretReceiver__: `receiver.BucketSecretReceiver`
* __ISecretStore__: `store.RamFsSecretStore`

The _akvd_ _systemd_ unit file has been configured to automatically populate the
service environment with an env file located at `/etc/akvd/runtime.env`. This is
useful for setting environment variables which can then be used in the
configuration for `selector.EnvAwareSecretSelector`. See the
__EnvAwareSecretSelector__ section for an example usage. The general recommended
strategy is to craft a single `appsettings.json` file which satisfies use needs
for all VMs and then customize the behavior on a per-role basis using the
`runtime.env` file.

Refer to the `appsettings.json` file included with this project.

The `appsettings.json` file has 4 main configuration sections corresponding to
`IAkvProvider`, `ISecretSelector`, `ISecretReceiver`, and `ISecretStore`. These
sections are entitled: `akv_provider`, `secret_selector`, `secret_receiver`,
and `secret_store`.

Each configuration section has the following format:

```json
{
  "name": "module",
  "options": {}
}
```

The `name` property tells _akvd_ which python class should be used to provide
the concrete implementation for that service. _akvd_ will first look in a the
following module for a concrete implementation:

| Service         | Module       |
| --------------- | ------------ |
| IAkvProvider    | akv          |
| ISecretSelector | selector     |
| ISecretReceiver | receiver     |
| ISecretStore    | store        |

For example, consider the following configuration section:

```json
...
"secret_selector": {
    "name": "EnvAwareSecretSelector",
    "options": {
        "pattern": "^%RCH_ENV%-(?P<name>(?:%RCH_ROLE%|common)-[0-9a-z-]+)"
    }
}
...
```

_akvd_ will first look in the `selector` module for a class called
`EnvAwareSecretSelector`.

If _akvd_ cannot resolve a class to the dedicated module for that class type, it
will assume the specified class is a fully qualified class name and will
attempt to resolve it accordingly. For example:

Given the following configuration:

```json
...
"akv_provider": {
    "name": "custom.NullAkvProvider",
    "options": {}
}
...
```

_akvd_ will first look for a fully qualified class `akv.custom.NullAkvProvider`.
If that class cannot be found (e.g. there is no file
`./akv/custom/null_akv_provider.py`), _akvd_ will try to resolve
`custom.NullAkvProvider`.

If _akvd_ cannot resolve a concrete implementation for any of the 4
extensibility points, it will fail to run.

Each of the 4 configurable classes take an `options` dictionary which is passed
verbatim to the concrete implementation. The concrete class can then extract
whatever options are meaningful for that particular concretion. For example,
not all `ISecretSelector` implementations must be provided an `options.pattern`
value, however, `EnvAwareSecretSelector` requires this value.

## Deployment

_WIP_

## Future Direction

* Complete `store.vendor.windows.ImDiskRamFsCreator`
* Change `store.vendor.windows.linux.LinuxRamFsCreator` to make its own _ramfs_
  always
* Purge local secrets from storage if they are not present in the key vault
  after each update cycle
* Move `appsettings.json` to `/etc/akvd`
