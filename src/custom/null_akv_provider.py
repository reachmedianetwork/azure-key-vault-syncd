"""
    This is an example of how you would create a "custom" Azure Key Vault provider. You would put your provider class
    here, export it in __init__.py and then change appsettings.json to:

    {
        ...
        "akv_provider": {
            "name": "custom.NullAkvProvider"
        }
        ...
    }
"""
from akv import IAkvProvider
from configuration import AppSettings


class NullAkvProvider(IAkvProvider):
    def __init__(self, app_settings: AppSettings, options: dict):
        super().__init__(app_settings, options)

    def list_secrets(self):
        return []

    def fetch(self, name: str) -> str:
        pass
