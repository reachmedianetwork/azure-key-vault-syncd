import abc


class IExecutor:
    """
    Defines the implementation requirements for an IExecutor. The IExecutor is the class which runs all the logic that
    akvd is responsible for. There are two concretions available:

    DaemonExecutor: runs an instance of RunOnceExecutor on an interval until SIGINT, SIGTERM, or SIGHUP is received
    RunOnceExecutor: runs the akvd application flow once

    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def execute(self) -> None:
        """
        runs the executor
        :return: None
        """
        pass
