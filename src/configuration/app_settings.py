from marshmallow import Schema, fields, post_load
import json


class SecretReceiverSettings(object):
    """
    secret_receiver app settings configuration block definition
    """
    def __init__(self, name: str, options: dict):
        """
        :param name: name of the provider to use
        :param options: provider specific configuration options
        """
        self.name = name
        self.options = options


class SecretReceiverSettingsSchema(Schema):
    """
    Schema for secret_receiver app settings configuration block
    """
    name = fields.Str(default="BucketSecretReceiver")
    options = fields.Dict(default={})

    @post_load
    def make_object(self, data):
        """
        Marshmallow post_load method to convert dict to SecretReceiverSettings object
        :param data: dict to convert
        :return: SecretReceiverSettings
        """
        return SecretReceiverSettings(**data)


class SecretStoreSettings(object):
    """
    secret_store app settings configuration block definition
    """
    def __init__(self, name: str, options: dict):
        """
        :param name: name of the provider to use
        :param options: provider specific configuration options
        """
        self.name = name
        self.options = options


class SecretStoreSettingsSchema(Schema):
    """
    Marshmallow schema for secret_store app settings configuration block
    """
    name = fields.Str(default="RamFsSecretStore")
    options = fields.Dict(default={})

    @post_load
    def make_object(self, data):
        """
        Marshmallow post_load method to convert dict to SecretStoreSettings object
        :param data: dict to convert
        :return: SecretStoreSettings
        """
        return SecretStoreSettings(**data)


class SecretSelectorSettings(object):
    """
    secret_selector app settings configuration block definition
    """
    def __init__(self, name: str, options: dict):
        """
        :param name: name of the provider to use
        :param options: provider specific configuration options
        """
        self.name = name
        self.options = options


class SecretSelectorSettingsSchema(Schema):
    """
    Marshmallow schema for secret_selector configuration block
    """
    name = fields.Str(default="EnvAwareSecretSelector")
    options = fields.Dict(default={})

    @post_load
    def make_object(self, data):
        """
        Marshmallow post_load method to convert dict to SecretSelectorSettings object
        :param data: dict to convert
        :return: SecretSelectorSettings
        """
        return SecretSelectorSettings(**data)


class AkvProviderSettings(object):
    """
    akv_provider app settings configuration block definition
    """
    def __init__(self, name: str, options: dict):
        """
        :param name: name of the provider to use
        :param options: provider specific options
        """
        self.name = name
        self.options = options


class AkvProviderSettingsSchema(Schema):
    """
    Marshmallow schema for akv_provider app settings configuration block
    """
    name = fields.Str(default="ManagedIdentityAkvProvider")
    options = fields.Dict(default={})

    @post_load
    def make_object(self, data):
        """
        Marshmallow post_load method to convert dict to AkvProviderSettings
        :param data: dict to convert
        :return: AkvProviderSettings
        """
        return AkvProviderSettings(**data)


class AppSettings(object):
    """
    Application settings for akvd
    """
    def __init__(
            self,
            akv_provider: AkvProviderSettings,
            secret_selector: SecretSelectorSettings,
            secret_store: SecretStoreSettings,
            secret_receiver: SecretReceiverSettings
    ):
        """
        :param akv_provider: settings specifying which IAkvProvider to use and provider options
        :param secret_selector: settings specifying which ISecretSelector to use and provider options
        :param secret_store: settings specifying which ISecretStore to use and provider options
        :param secret_receiver: settings specifying which ISecretReceiver to use and provider options
        """
        self.akv_provider = akv_provider
        self.secret_selector = secret_selector
        self.secret_store = secret_store
        self.secret_receiver = secret_receiver


class AppSettingsSchema(Schema):
    """
    Marshmallow schema for application settings
    """
    akv_provider = fields.Nested(AkvProviderSettingsSchema)
    secret_selector = fields.Nested(SecretSelectorSettingsSchema)
    secret_store = fields.Nested(SecretStoreSettingsSchema)
    secret_receiver = fields.Nested(SecretReceiverSettingsSchema)

    @post_load
    def make_object(self, data):
        """
        Marshmallow post_load method to convert dict to AppSettings
        :param data: dict to convert
        :return: AppSettings
        """
        return AppSettings(**data)


def load(filename: str) -> AppSettings:
    """
    Helper function to load application settings from a JSON file on disk
    :param filename: filename to load the application settings from
    :return: AppSettings
    """
    schema = AppSettingsSchema()
    with open(filename, 'r') as file:
        file_data = file.read()
        json_data = json.loads(file_data)

    return schema.load(json_data).data
