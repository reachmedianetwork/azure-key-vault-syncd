from argparse import ArgumentParser
import pydoc

import akv
from configuration import AppSettings, load
from executor import DaemonExecutor, RunOnceExecutor
from log import Log
import selector
import store
import receiver
from util import get_object_module_name

logger = Log.create(__name__)


def create_executor(daemon=False, interval=900):
    """
    :param daemon: whether or not to run the executor as a daemon
    :param interval: interval, in seconds, to refresh secrets (only used
        when run as a daemon)
    :return: the executor
    :rtype: IExecutor
    """
    logger.debug("loading appsettings.json")
    settings = load("appsettings.json")  # type: AppSettings

    # instantiate akv_provider
    try:
        akv_provider_cls = getattr(akv, settings.akv_provider.name)
    except AttributeError:
        akv_provider_cls = pydoc.locate(settings.akv_provider.name)

    logger.debug(
        "using IAkvProvider `{0}` from module {1}"
        .format(akv_provider_cls.__name__, get_object_module_name(akv_provider_cls))
    )

    akv_provider = akv_provider_cls(settings, settings.akv_provider.options)

    # instantiate secret_selector
    try:
        secret_selector_cls = getattr(selector, settings.secret_selector.name)
    except AttributeError:
        secret_selector_cls = pydoc.locate(settings.secret_selector.name)

    logger.debug(
        "using ISecretSelector `{0}` from module {1}"
        .format(secret_selector_cls.__name__, get_object_module_name(secret_selector_cls))
    )

    secret_selector = secret_selector_cls(settings, settings.secret_selector.options)

    # instantiate secret_store
    try:
        secret_store_cls = getattr(store, settings.secret_store.name)
    except AttributeError:
        secret_store_cls = pydoc.locate(settings.secret_store.name)

    logger.debug(
        "using ISecretStore `{0}` from module {1}"
        .format(secret_store_cls.__name__, get_object_module_name(secret_store_cls))
    )

    secret_store = secret_store_cls(settings, settings.secret_store.options)

    # instantiate receiver
    try:
        secret_receiver_cls = getattr(receiver, settings.secret_receiver.name)
    except AttributeError:
        secret_receiver_cls = pydoc.locate(settings.secret_receiver.name)

    logger.debug(
        "using ISecretReceiver `{0}` from module {1}"
        .format(secret_receiver_cls.__name__, get_object_module_name(secret_receiver_cls))
    )

    secret_receiver = secret_receiver_cls(secret_store, settings, settings.secret_receiver.options)

    executor = RunOnceExecutor(akv_provider, secret_selector, secret_receiver)
    if daemon:
        logger.info("running in daemon mode")
        executor = DaemonExecutor(interval, executor)
    else:
        logger.info("running in cli mode")

    return executor


def main():
    """
    Main Program
    """
    parser = ArgumentParser(
        description='Sync secrets from Azure Key Vault to local file system.')
    parser.add_argument('--daemon', action='store_true')
    parser.add_argument('--interval', type=int, default=900,
                        help='interval, in seconds, to refresh secrets')

    args = parser.parse_args()

    logger.info("azure-key-vault-syncd v0.1")

    executor = create_executor(daemon=args.daemon, interval=args.interval)
    executor.execute()


if __name__ == '__main__':
    main()
