import abc
from typing import Tuple
from configuration import AppSettings


class ISecretSelector:
    """
    Abstract class defining implementation requirement for secret_selector. An ISecretSelector must test a secret
    name and return a result indicating if the secret should be selected and if so, what secret name should be sent
    to the ISecretReceiver which may or may not be the same as the original secret name.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, app_settings: AppSettings, options: dict):
        """
        Silly hack to force concretions to accept AppSettings and kwargs
        :param app_settings: AppSettings to use
        :param options: provider specific options
        """
        pass

    @abc.abstractmethod
    def is_secret_selected(self, secret_name) -> Tuple[bool, str, str]:
        """
        Tests if a secret should be selected for fetching.

        The first value in the tuple is a boolean indicating if the secret should be selected or not.
        The second value in the tuple is the original name of the secret
        The second value in the tuple is the mutated name of the secret (what it should be stored as locally)

        :param secret_name: name of secret to test
        :return: Tuple[bool, str, str]
        """
        pass
