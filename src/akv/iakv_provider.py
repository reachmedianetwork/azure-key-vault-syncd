import abc
from typing import List
from configuration import AppSettings


class IAkvProvider:
    """
    An abstract class defining implementation requirements for an Azure Key Vault Provider (AKV provider). An AKV
    provider must be able to enumerate secrets and fetch secret values.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, app_settings: AppSettings, options: dict):
        """
        Silly hack to force concretions to accept AppSettings and kwargs. This is so that the application bootstrap
        can reliably construct concretions.

        :param app_settings: an instance of the AppSettings loaded by bootstrap
        :param options: a dict containing arbitrary options for the concretion
        """
        pass

    @abc.abstractmethod
    def list_secrets(self) -> List[str]:
        """
        lists all secrets from configured azure key vault
        :return: list of secret names available to the application
        """
        pass

    @abc.abstractmethod
    def fetch(self, name: str) -> str:
        """
        fetches the value of a secret
        :param name: name of the secret to fetch
        :return: value of the secret
        """
        pass
