from .iakv_provider import IAkvProvider
from marshmallow import Schema, fields, post_load
import httplib2
import os
import datetime
from datetime import timezone
import json
import base64
from typing import Any, TYPE_CHECKING, Optional, List
from azure.keyvault.secrets import SecretClient, KeyVaultSecret
from azure.core.credentials import AccessToken
from configuration import AppSettings
from log import Log
from util import expand

logger = Log.create(__name__)


class ManagedIdentityAkvProviderSettings(object):
    """
    Options for ManagedIdentityAkvProvider
    """
    def __init__(self, vault_url: str):
        """
        :param vault_url: URL of the Azure Key Vault to access
        """
        self.vault_url = expand(vault_url)


class ManagedIdentityAkvProviderSettingsSchema(Schema):
    """
    Marshmallow schema for ManagedIdentityAkvProviderSettings
    """
    vault_url = fields.Str(required=True)

    @post_load
    def make_object(self, data) -> ManagedIdentityAkvProviderSettings:
        """
        post_load action to convert dict data to ManagedIdentityAkvProviderSettings object
        :param data: dict data to convert
        :return:
        """
        return ManagedIdentityAkvProviderSettings(**data)


class AimsResponse(object):
    """
    Describes a response from the Azure Instance Metadata Service
    """

    def __init__(
            self, access_token: str, client_id: str, expires_in: int, expires_on: int, ext_expires_in: int,
            not_before: int, resource: str, token_type: str
    ):
        """
        https://docs.microsoft.com/en-us/azure/active-directory/managed-identities-azure-resources/how-to-use-vm-token

        :param access_token: Bearer JWT
        :param client_id: the client_id of the managed identity
        :param expires_in: the number of seconds the access token continues to be valid,from time of issuance
        :param expires_on: the timespan (unix offset) when the access token expires
        :param ext_expires_in: not documented by MS
        :param not_before: the timespan (unix offset) when the access token takes effect, and can be accepted
        :param resource: the resource the access token was requested for
        :param token_type: The type of token, which is a "Bearer" access token
        """
        self.access_token = access_token
        self.client_id = client_id
        self.expires_in = expires_in
        self.expires_on = expires_on
        self.ext_expires_in = ext_expires_in
        self.not_before = not_before
        self.resource = resource
        self.token_type = token_type


if TYPE_CHECKING:
    from azure.core.credentials import TokenCredential

    class AkvsdTokenCredential(TokenCredential):
        """
        TokenCredential implementation that can be constructed from an AimsResponse object
        """
        def __init__(self, aims_response: AimsResponse):
            """
            Constructs an AkvsdTokenCredential from an AimsResponse object
            :param aims_response: response object from which to construct the token
            """
            self.token = AccessToken(aims_response.access_token, aims_response.expires_on)

        def get_token(self, *scopes: str, **kwargs: Any) -> AccessToken:
            """
            Returns an AccessToken
            :param scopes: not used
            :param kwargs: not used
            :return: AccessToken
            """
            return self.token
else:
    class AkvsdTokenCredential(object):
        """
        TokenCredential implementation that can be constructed from an AimsResponse object
        """
        def __init__(self, aims_response: AimsResponse):
            """
            Constructs an AkvsdTokenCredential from an AimsResponse object
            :param aims_response: response object from which to construct the token
            """
            self.token = AccessToken(aims_response.access_token, aims_response.expires_on)

        def get_token(self, *scopes: str, **kwargs: Any) -> AccessToken:
            """
            Returns an AccessToken
            :param scopes: not used
            :param kwargs: not used
            :return: AccessToken
            """
            return self.token


class AimsResponseSchema(Schema):
    """
    Marshmallow schema for AimsResponse object
    """
    access_token = fields.Str()
    client_id = fields.Str()
    expires_in = fields.Int()
    expires_on = fields.Int()
    ext_expires_in = fields.Int()
    not_before = fields.Int()
    resource = fields.Str()
    token_type = fields.Str()

    @post_load
    def make_aims_response(self, data) -> AimsResponse:
        """
        post_load Marshmallow action to convert dict to AImsResponse
        :param data: dict to convert to AimsResponse
        :return: AimsResponse
        """
        return AimsResponse(**data)


class ManagedIdentityAkvProvider(IAkvProvider):
    """
    An Azure Key Vault provider which uses the Azure Instance Metadata Service to acquire a access token for an
    Azure Key Vault. For this to properly work, the VM that akvd is running on must have a single managed identity
    assigned to it and that managed identity must be granted the GET and LIST rights to the target Azure Key Vault.
    """
    options = None  # type: Optional[ManagedIdentityAkvProviderSettings]
    aims_response = None  # type: Optional[AimsResponse]

    def __init__(self, app_settings: AppSettings, options: dict):
        super().__init__(app_settings, options)

        schema = ManagedIdentityAkvProviderSettingsSchema()
        self.options = schema.load(options).data

    def check_aims_response(self):
        """
        Deletes the internal aims_response if it will expire ~soon~(ish). This will induce a new token to be acquired
        during the next update cycle.
        :return: None
        """
        if self.aims_response is not None:
            dt = datetime.datetime.now()

            utc_time = dt.replace(tzinfo=timezone.utc)
            utc_timestamp = utc_time.timestamp()

            if utc_timestamp > self.aims_response.expires_on - 30:
                logger.warning("stored AIMS response is expiring soon; removing it")
                self.aims_response = None

    @staticmethod
    def json_string_to_token(json_string: str) -> AimsResponse:
        """
        Converts a JSON-formatted string (e.g. API response from AIMS) into an AimResponse object
        :param json_string: string to convert
        :return: AimsResponse
        """
        decoded_aat = json.loads(json_string)
        schema = AimsResponseSchema()
        return schema.load(decoded_aat).data

    @staticmethod
    def get_secret_value(secret: KeyVaultSecret):
        """
        Converts a KeyVaultSecret into a string value.

        This method will automatically convert secret values based on the content_type property (if present) of the
        secret. The content_type property is expected to be in the following format: <mimetype>;<encoding>. For example:

        Given content_type is `text/plain;base64`, this method will base64 decode the input secret value and return the
        decoded string.

        This method does not currently do anything regarding the mimetype portion of the content_type. This is reserved
        for future expansion.

        :param secret: KeyVaultSecret to convert
        :return: string value of the secret
        """
        if secret.properties.content_type is not None:
            encoding = secret.properties.content_type.split(";")[-1]

            if encoding == "base64":
                return base64.b64decode(secret.value.encode("utf8")).decode("utf8")

        return secret.value

    def get_access_token(self) -> AimsResponse:
        """
        Acquires and returns a valid AIMS access token. If the environment variable AIMS_ACCESS_TOKEN is set, this
        method will use that as the JSON-encoded string and will not make a call to the AIMS service. This is intended
        for development purposes only.

        Otherwise, the provider will access AIMS to acquire an access_token for Azure Key Vault. This token will be
        stored internally and re-used until the token expires. The token will be discarded 30 seconds before its
        expiration to reduce the likelihood of the token expiring during use.

        :return: AimsResponse
        """

        self.check_aims_response()

        if self.aims_response is None:
            try:
                self.aims_response = ManagedIdentityAkvProvider.json_string_to_token(os.environ['AIMS_ACCESS_TOKEN'])

                logger.warning("retrieved AIMS response from env; this should be used for development only")
            except KeyError:
                logger.info("requesting a bearer token from AIMS")

                http = httplib2.Http()
                resp, content = http.request(
                    "http://169.254.169.254/metadata/identity/oauth2/token"
                    "?api-version=2019-11-01&resource=https://vault.azure.net",
                    headers={"Metadata": "true"}
                )

                self.aims_response = ManagedIdentityAkvProvider.json_string_to_token(content.decode("utf-8"))

                logger.info("received a token from AIMS which expires on {0}".format(self.aims_response.expires_on))

        return self.aims_response

    def list_secrets(self) -> List[str]:
        """
        lists all the secrets available in the key vault
        :return: List[str]
        """
        access_token = self.get_access_token()

        logger.debug("accessing keyvault at {0}".format(self.options.vault_url))

        secret_client = SecretClient(self.options.vault_url, AkvsdTokenCredential(access_token))
        secret_properties = secret_client.list_properties_of_secrets()

        result = [prop.name for prop in secret_properties]

        logger.debug("list_secrets successful: {0}".format(", ".join(result)))

        return result

    def fetch(self, name: str) -> str:
        """
        fetches the string value of a secret from the vault
        :param name: secret name to fetch
        :return:
        """
        access_token = self.get_access_token()

        secret_client = SecretClient(self.options.vault_url, AkvsdTokenCredential(access_token))
        secret = secret_client.get_secret(name)

        logger.debug("successfully retrieved value for secret `{0}`".format(name))

        return ManagedIdentityAkvProvider.get_secret_value(secret)
