import abc
from configuration import AppSettings


class ISecretStore:
    """
    Defines the implementation requirements for secret_store. An ISecretStore must be able to save and evict a secret.
    The ISecretStore must also be able to ensure that a secret "category" can only be enumerated be a specific OS
    group.
    """
    __metaclass__ = abc.ABCMeta

    @abc.abstractmethod
    def __init__(self, app_settings: AppSettings, options: dict):
        """
        Silly hack to force concretions to accept AppSettings and kwargs
        :param app_settings: AppSettings to use
        :param options: provider options
        """
        pass

    @abc.abstractmethod
    def save(self, category: str, name: str, user: str, group: str, data) -> None:
        """
        saves a secret to the secret store. This is, typically, a file on a file system of some kind.

        :param category: the category of the secret (e.g. web, worker, etc.)
        :param name: name of the secret (e.g. mysql-connection-string)
        :param user: the OS user to secure the secret to
        :param group: the OS group to secure the secret to
        :param data: the value of the secret
        :return: None
        :raises: when the underlying file system handler raises
        """
        pass

    @abc.abstractmethod
    def evict(self, category: str, name: str) -> None:
        """
        removes a secret `name` from `category`
        :param category: the category from which to remove the secret
        :param name: the name of the secret to remove
        :return: None
        :raises: when the underlying file system handler raises
        """
        pass

    @abc.abstractmethod
    def ensure_secure(self, category: str, group: str) -> None:
        """
        ensures that a secret category can only be read by `group`.
        :param category: the category to secure
        :param group: the group to permit list access to
        :return:
        """
        pass
